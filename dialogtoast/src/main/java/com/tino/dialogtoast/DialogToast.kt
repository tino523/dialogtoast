package com.tino.dialogtoast

import android.app.Dialog
import android.content.Context
import android.os.Handler
import android.os.Looper
import android.text.TextUtils
import android.widget.TextView
import androidx.annotation.StringRes

class DialogToast private constructor(context: Context) {

    private lateinit var mContext: Context
    private var mHandler = Handler(Looper.getMainLooper())
    private lateinit var mTextView: TextView
    private var mDuration = LENGTH_SHORT
    private lateinit var mDialog: Dialog

    companion object {
        const val LENGTH_SHORT = 1500L
        const val LENGTH_LONG = 3000L

        fun makeText(context: Context, message: CharSequence, @DialogToastAnnotation duration: Long = LENGTH_SHORT): DialogToast {
            val dialogToast = DialogToast(context)
            try {
                dialogToast.mDuration = duration
                dialogToast.mTextView.text = message
            } catch (e: Exception) {
                e.printStackTrace()
            }
            return dialogToast
        }

        fun makeText(context: Context, @StringRes resId: Int, @DialogToastAnnotation duration: Long = LENGTH_SHORT): DialogToast {
            var message = ""
            try {
                message = context.resources.getString(resId)
            } catch (e: Exception) {
                e.printStackTrace()
            }
            return makeText(context, message, duration)
        }
    }

    init {
        try {
            if (null != context) {
                mContext = context
                mDialog = Dialog(mContext, R.style.DialogToastStyle)
                mDialog.setContentView(R.layout.activity_dialog_toast)
                mTextView = mDialog.findViewById(R.id.message_view)
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    fun show() {
        try {
            if (!TextUtils.isEmpty(mTextView.text)) {
                mDialog.show()
                mHandler.postDelayed({
                    mDialog.dismiss()
                }, mDuration)
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

//    @LongDef(DialogToast.LENGTH_SHORT, DialogToast.LENGTH_LONG)
//    @Target(AnnotationTarget.VALUE_PARAMETER)
//    @kotlin.annotation.Retention(AnnotationRetention.SOURCE)
//    private annotation class DialogToastAnnotation
}