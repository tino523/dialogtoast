package com.tino.dialogtoast;

import androidx.annotation.LongDef;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@LongDef({DialogToast.LENGTH_SHORT, DialogToast.LENGTH_LONG})
@Target(ElementType.PARAMETER)
@Retention(RetentionPolicy.SOURCE)
public @interface DialogToastAnnotation {}